var path = require('path');
var gulp = require('gulp');
var less = require('gulp-less');
var concat = require('gulp-concat');
var server = require('gulp-express');
var babel = require("gulp-babel");
var sourcemaps = require("gulp-sourcemaps");
var bower = require('bower-files')();
var uglify = require('gulp-uglify');

gulp.task('default', function() {

});

var config = {
    bowerDir: 'bower_components'
};

gulp.task('babel', function(){

    return gulp.src('src/js/**/*.js')
        .pipe(sourcemaps.init())
        .pipe(babel({
            modules: 'amd'
        }))
        //.pipe(concat('main.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('public/js'))
    ;
});

gulp.task('js:bower', function(){

    return gulp.src(bower.ext('js').files)
        .pipe(concat('lib.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/js/lib'))
    ;
});

gulp.task('js:bower:require', function(){

    return gulp.src(config.bowerDir + '/requirejs/require.js')
        .pipe(gulp.dest('public/js'))
        ;
});

gulp.task('bootstrap:css', function(){

    return gulp.src(path.join(config.bowerDir + '/bootstrap/dist/css/**.*'))
        .pipe(gulp.dest('public/css'))
        ;
});

gulp.task('font:bower', function(){

    return gulp.src(path.join(config.bowerDir + '/bootstrap/dist/fonts/**.*'))
        .pipe(gulp.dest('public/fonts'))
    ;
});

gulp.task('less', function () {
    return gulp.src('src/styles/less/**/*.{less,css}')
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(concat('all.css'))
        .pipe(gulp.dest('public/css'));
});

gulp.task('server', ['babel', 'less', 'js:bower', 'js:bower:require', 'bootstrap:css', 'font:bower'], function() {

    server.run(['bin/www']);

    gulp.watch(['views/**/*.{html,jade}'], server.notify);

    gulp.watch(['src/styles/less/**/*.{less,css}'], ['less']);

    gulp.watch(['src/js/**/*.js'], function(e){

        if(e.type === 'changed'){

            var relativePath = path.relative(path.join(__dirname, 'src'), e.path);
            var targetPath = path.dirname(path.join('public', relativePath));

            gulp.src([e.path])
                .pipe(sourcemaps.init())
                .pipe(babel({
                    modules: 'amd'
                }))
                .pipe(sourcemaps.write('.'))
                .pipe(gulp.dest(targetPath))
            ;
        }
    });

    gulp.watch(['public/img/**/*'], server.notify);
});
