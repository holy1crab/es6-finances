
import {Controller} from 'controller';
import * as dom from 'util/dom';

export class TagEditor extends Controller {

    constructor(options){
        super(options);

        this.tags = ['bird', 'abc', 'somelongname'];
    }

    render(){

        this.domNode = dom.declare('div', {
            style: 'border: 1px solid black; width: 300px; height: 200px;'
        });

        this.domContent = dom.declare('div', {
            contenteditable: true,
            innerHTML: '',
            style: 'width: 100%; height: 100%;'
        });

        this.domNode.appendChild(this.domContent);

        this.options.parentNode.appendChild(this.domNode);

        this.domContent.addEventListener('input', this._onContentInput.bind(this), false);

        return this;
    }

    _onContentInput(e){
        console.log(e);
    }
}
