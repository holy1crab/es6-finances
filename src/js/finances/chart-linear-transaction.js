
import * as util from 'util/util';

const ns = 'http://www.w3.org/2000/svg';

export function getChart(options){

    options = options || {};
    let events = options.events || {};

    let width = 800;
    let height = 220;
    let margin = {
        top: 20,
        right: 50,
        bottom: 20,
        left: 75
    };

    let balance = 0;

    let balanceData = options.data.map(x => {

            if(x.add){
                balance += x.sum;
            }else{
                balance -= x.sum;
            }

            return {
                dateCreated: x.dateCreated,
                balance: balance,
                add: x.add
            };
        });

    //let inData = options.data.filter(x => x.add).map(x => { return x; });
    //let outData = options.data.filter(x => !x.add);
    let svg = document.createElementNS(ns, 'svg');

    svg.setAttribute('class', 'chart-linear-transaction');

    let g = d3.select(svg)
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    var balanceMinValue = d3.min(balanceData, x => x.balance);

    let x = d3.time.scale().range([0, width]).domain(d3.extent(balanceData, x => x.dateCreated));
    let y = d3.scale.linear().range([height, 0]);

    if(balanceMinValue >= 0){
        y.domain([0, d3.max(balanceData, x => x.balance)]);
    } else {
        y.domain(d3.extent(balanceData, x => x.balance));
    }

    let line = d3.svg.line()
        .interpolate('basis')
        .x(d => x(d.dateCreated))
        .y(d => y(d.balance));

    let lineGroup = g.append('g')
            .attr('class', 'line-group')
        ;

    var rectSelection;

    lineGroup.append('rect')
            .attr('x', 0)
            .attr('y', 0)
            .attr('width', width)
            .attr('height', height)
            .attr('fill-opacity', 0)
            .on('mousedown', () => {

                let offsetLeft = d3.event.clientX - margin.left;
                let initialClientX = d3.event.clientX;

                let dateBegin = x.invert(offsetLeft);

                if(typeof events.onSelectionMove === 'function'){
                    events.onSelectionMove(null, { dateBegin: dateBegin });
                }

                if(rectSelection){
                    rectSelection.remove();
                }

                rectSelection = lineGroup.append('rect')
                        .attr('x', offsetLeft)
                        .attr('y', 0)
                        .attr('width', 0)
                        .attr('height', height)
                        .attr('fill-opacity', 0.1)
                        .on('mousedown', d => rectSelection.remove())
                    ;

                var onMouseMove = function(e){

                    if(e.clientX <= margin.left - 2 || e.clientX >= margin.left + width + 2){
                        return;
                    }

                    let dateEnd = x.invert(e.clientX - margin.left);

                    if(initialClientX < e.clientX){
                        rectSelection.attr('width', e.clientX - offsetLeft - margin.left);
                    } else {
                        rectSelection.attr('x', e.clientX - margin.left);
                        rectSelection.attr('width', initialClientX - e.clientX);
                    }

                    if(typeof events.onSelectionMove === 'function'){
                        events.onSelectionMove(null, { dateBegin: dateBegin, dateEnd: dateEnd });
                    }
                };

                document.body.addEventListener('mousemove', onMouseMove, false);

                document.body.addEventListener('mouseup', () => {
                    document.body.removeEventListener('mousemove', onMouseMove, false);
                });
            })
        ;

    let path = lineGroup.append('path')
            .datum(balanceData)
            .attr('class', 'line')
            .attr('d', line)
        ;

    let xAxis = d3.svg.axis()
        .scale(x)
        .tickFormat(d3.time.format('%d.%m'))
        .orient('bottom')
        ;

    let yAxis = d3.svg.axis()
            .scale(y)
            .orient('left')
            .ticks(5)
        ;

    let selAxisX = g.append('g')
        .attr('class', 'x axis')
        .attr('transform', 'translate(0,' + height + ')')
        .call(xAxis);

    // remove first tick
    selAxisX.select('.tick').remove();

    g.append('g')
        .attr('class', 'y axis')
        .call(yAxis)
        .append('text')
        .attr('transform', 'rotate(-90)')
        .attr('y', 6)
        .attr('dy', '.71em')
        .style('text-anchor', 'end')
        .text('Баланс');

    var circlesGroup = g.append('g');

    balanceData.forEach(o => {

        var cx = x(o.dateCreated);
        var cy = y(o.balance);

        var svgTooltip = null;

        circlesGroup.append('circle')
            .attr('cx', cx)
            .attr('cy', cy)
            .attr('r', 3)
            .attr('stroke', 'steelblue')
            .attr('stroke-width', 2)
            .attr('fill', o.add ? '#86FF4A' : 'white')
            .on('mouseover', () => {

                svgTooltip = circlesGroup.node().appendChild(makeTooltip(cx + 5, cy, 74, 30, o.balance));
            })
            .on('mouseleave', () => {
                svgTooltip.parentNode.removeChild(svgTooltip);
            })
        ;

        circlesGroup.append('text')
            .attr('class', 'arrow-add')
            .attr('x', cx - 3)
            .attr('y', o.add ? cy - 10 : cy + 15)
            .text(o.add ? '↑' : '↓')
        ;
    });

    return svg;

}

function makeTooltip(x, y, width, height, text){

    let g = d3.select(document.createElementNS(ns, 'g'));

    g.append('text')
        .attr('class', 'balance-tooltip')
        .attr('x', x + 5)
        .attr('y', y - 10)
        .text(text)
    ;

    return g.node();
}