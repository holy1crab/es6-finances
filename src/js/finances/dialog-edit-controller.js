
import {Controller} from 'controller';
import * as dom from 'util/dom';
import * as util from 'util/util'
import * as currency from 'finances/model/currency'


export class ControllerDialogEdit extends Controller {

    constructor(options){
        super(options);

        this.item = this.options.item || null;
        this.tags = this.options.tags || [];
        this.controls = [];
        this.events = this.options.events || {};

        this._currencyValue = null;
    }

    render(){

        this.domNode = dom.declare('div', {
            className: 'modal bs-example-modal-lg'
        });

        this.domNode.innerHTML = [

            '<div class="modal-dialog" style="width: 40%; max-width: 550px;">',
                '<div class="modal-content">',
                    '<div class="modal-header">',
                        '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
                        '<h4 class="modal-title">', this.options.title, '</h4>',
                    '</div>',
                    '<div class="modal-body">',

                        '<form>',
                            '<div class="form-group">',
                                '<label>Когда</label>',
                                '<input type="text" class="form-control date" placeholder="Дата">',
                                '<p class="help-block">Примеры: 31.12.2014, 24.02.2015 21:51</p>',
                            '</div>',
                            '<div class="form-group">',
                                '<div class="btn-group add" data-toggle="buttons">',
                                    '<label class="btn btn-default">',
                                        '<input type="radio" name="add" autocomplete="off" value="0"> Списание',
                                    '</label>',
                                    '<label class="btn btn-default">',
                                        '<input type="radio" name="add" autocomplete="off" value="1"> Пополнение',
                                    '</label>',
                                '</div>',
                            '</div>',
                            '<div class="form-group">',
                                '<label>Сколько</label>',
                                '<input type="text" class="form-control sum" placeholder="Сумма">',
                            '</div>',
                            '<div class="form-group" id="currency-edit-container"></div>',
                            '<div class="form-group">',
                                '<label>На что / Откуда</label>',
                                '<div id="tags-edit-container">',
                                    '<input type="text" class="form-control tag" placeholder="Теги">',
                                    '<p class="help-block">Разделенные запятыми</p>',
                                '</div>',
                            '</div>',
                        '</form>',

                    '</div>',
                    '<div class="modal-footer">',
                        '<button type="button" class="btn btn-default" data-dismiss="modal">Отменить</button>',
                        '<button type="button" class="btn btn-primary">Сохранить</button>',
                    '</div>',
                '</div>',
            '</div>'

        ].join('');

        var currencyOptions = Object.keys(currency.Currency).map(x => {

            var value = currency.Currency[x];

            return {
                value: value,
                text: currency.getHumanName(value),
                selected: this.item && this.item.currency === value
            };
        });

        if(!this.item){
            currencyOptions[0].selected = true;
        }

        var domCurrency = ControllerDialogEdit.getDomDropDown(currencyOptions, {
            placeholder: 'Валюта',
            changed: this._onCurrencyChanged.bind(this)
        });

        this.domNode.querySelector('#currency-edit-container').appendChild(domCurrency);

        this.domInputDate = this.domNode.querySelector('.date');
        this.domInputsAdd = this.domNode.querySelectorAll('.add input');
        this.domInputSum = this.domNode.querySelector('.sum');
        this.domInputTag = this.domNode.querySelector('.tag');
        this.domButtonSave = this.domNode.querySelector('.modal-footer .btn-primary');

        if(this.item){
            this.domInputDate.value = util.dateFormat(this.item.dateCreated);
            this.domInputSum.value = this.item.sum;

            this.domInputsAdd[this.item.add ? 1 : 0].checked = true;
            this.domInputsAdd[this.item.add ? 1 : 0].parentNode.classList.add('active');

            this.domInputTag.value = this.tags.join(', ');

        } else {
            this.domInputDate.value = util.dateFormat(new Date());
            this.domInputSum.value = '0.00';
            this.domInputsAdd[0].checked = true;
            this.domInputsAdd[0].parentNode.classList.add('active');

            this.domInputTag.value = '';
        }

        this.options.parentNode.appendChild(this.domNode);

        this._bindEvents();

        return this;
    }

    _bindEvents(){
        $(this.domNode).on('hidden.bs.modal', this._onModalHidden.bind(this));

        this.domButtonSave.addEventListener('click', this._onButtonSaveClick.bind(this), false);
    }

    _onCurrencyChanged(sender, args){

        this._currencyValue = args.value;
    }

    _onButtonSaveClick(e){

        var dateCreated = util.dateParse(this.domInputDate.value);
        var add = Array.prototype.slice.call(this.domInputsAdd, 0).filter(x => x.checked)[0].value === '1';
        var sum = parseInt(this.domInputSum.value, 10);
        var tags = this.domInputTag.value.split(',').map(x => x.trim()).filter(x => x);

        if(sum <= 0 || dateCreated.toString().toLowerCase() === 'invalid date' || tags.length === 0){

            alert('Incorrect input values');

        } else {

            if(typeof this.events.onSave === 'function'){

                this.events.onSave(this, {
                    data: {
                        dateCreated: dateCreated,
                        add:add,
                        sum: sum,
                        tags: tags,
                        currency: this._currencyValue
                    },
                    item: this.item,
                    e: e
                });
            }
        }
    }

    static getDomDropDown(options, settings){

        var domNode = dom.declare('div', {
            className: 'dropdown',
            innerHTML: [
                '<div class="dropdown">',
                    '<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">',
                        '<span class="selected-value" style="margin-right: 10px;">', settings.placeholder, '</span>',
                        '<span class="caret"></span>',
                    '</button>',
                    '<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">',
                        options.map(x => {
                            return ['<li data-value="', x.value, '" ', (x.selected ? 'class="selected"' : ''), '><a>', x.text, '</a></li>'].join('');
                        }).join(''),
                    '</ul>',
                '</div>'
            ].join('')
        });

        var optionsDict = {};
        options.forEach(x => {
            return optionsDict[x.value] = x;
        });

        var $menu = $('.dropdown-menu', domNode);
        var $selectedText = $('.selected-value', domNode);
        var $selected = $menu.find('.selected');
        if($selected.length){
            var value = $selected.data('value');
            $selectedText.text(optionsDict[value].text);
            settings.changed(domNode, { value: value });
        }

        $menu.on('click', 'li', function(e){
            var value = e.currentTarget.getAttribute('data-value');
            $selectedText.text(optionsDict[value].text);

            if(typeof settings.changed === 'function'){
                settings.changed(domNode, { value: value });
            }
        });

        return domNode;
    }

    _onModalHidden(){
        if(typeof this.events.onHidden === 'function'){
            this.events.onHidden(this);
        }
    }

    show(){
        $(this.domNode).modal('show');
    }

    hide(){
        $(this.domNode).modal('hide');
    }
}
