
import { IncFactory } from 'util/util';

var inc = IncFactory.create('transaction-tag');

export class TransactionTag {

    constructor(options){

        this.tagId = options.tagId;
        this.transactionId = options.transactionId;
    }

}
