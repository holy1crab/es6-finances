
import * as util from 'util/util';
import {Manager} from 'finances/model/manager';
import {Transaction} from 'finances/model/transaction';

export class TransactionManager extends Manager {

    constructor(options){
        super(options);

        this.count = 0;
    }

    mapItem(x){

        x.dateCreated = new Date(x.dateCreated);

        return new Transaction(x);
    }

    list(options){

        options = options || {};

        let els = this._cached;

        let range = options.range || {};
        let filter = options.filter || {};

        if(filter.dateCreatedBegin && filter.dateCreatedEnd) {
            els = els.filter(x => x.dateCreated >= filter.dateCreatedBegin && x.dateCreated <= filter.dateCreatedEnd);
        } else if(filter.dateCreatedBegin) {
            els = els.filter(x => x.dateCreated >= filter.dateCreatedBegin);
        } else if(filter.dateCreatedEnd) {
            els = els.filter(x => x.dateCreated <= filter.dateCreatedEnd);
        }

        this.count = els.length;

        if(options.range){
            els = els.slice(range.offset, range.offset + range.limit);
        }

        return els;
    }
}