
import { IncFactory } from 'util/util';

var inc = IncFactory.create('transaction');

export class Transaction {

    constructor(options){
        this.id = options.id || inc();

        this.dateCreated = options.dateCreated;
        this.dateChanged = options.dateChanged || null;

        this.sum = options.sum;
        this.currency = options.currency;
        this.add = options.add; // debit or credit
    }
}