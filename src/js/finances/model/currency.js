
export var Currency = {
    Rub: 'RUB',
    Usd: 'USD',
    Eur: 'EUR',
    Cny: 'CNY'
};

var human = {};
human[Currency.Rub] = 'Рубль';
human[Currency.Usd] = 'Доллар';
human[Currency.Eur] = 'Евро';
human[Currency.Cny] = 'Юань';

var sign = {};
sign[Currency.Rub] = '₽';
sign[Currency.Usd] = '$';
sign[Currency.Eur] = '€';
sign[Currency.Cny] = 'Y';

export function getHumanName(currency){
    return human[currency];
}

export function getSign(currency){
    return sign[currency];
}
