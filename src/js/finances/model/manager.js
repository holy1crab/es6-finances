
export class Manager {

    constructor(options){

        this.storage = localStorage;
        this.table = options.table;

        this._cached = null;
    }

    mapItem(){
        throw new Error('not implemented');
    }

    init(){
        var objects = JSON.parse(this.storage.getItem(this.table));
        this._cached = objects ? objects.map(x => this.mapItem(x)) : [];
    }

    get(id){
        return this._cached.filter(x => x.id === id)[0];
    }

    list(){
        return this._cached;
    }

    add(...items){

        this._cached.push(...items);

        this.storage.setItem(this.table, JSON.stringify(this._cached));
    }

    save(item) {

        var elements = this._cached.filter(x => x.id === item.id);

        if(elements.length > 0){
            var index = this._cached.indexOf(elements[0]);
            this._cached.splice(index, 1, item);
        }

        this.storage.setItem(this.table, JSON.stringify(this._cached));
    }

    remove(){

    }

    drop(){
        this.storage.removeItem(this.table);
        this._cached = [];
    }
}
