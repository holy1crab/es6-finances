
import {Manager} from 'finances/model/manager';
import {TransactionTag} from 'finances/model/transaction-tag';

export class TransactionTagManager extends Manager {

    constructor(options){
        super(options);
    }

    mapItem(x){

        return new TransactionTag(x);
    }

    get(transactionId, tagId){
        var els = this._cached.filter(x => x.transactionId === transactionId && x.tagId === tagId);
        return els.length > 0 ? els[0] : null;
    }

    list(options){

        options = options || {};

        let filter = options.filter || {};

        let elements = this._cached;

        if(filter.transactionId){
            elements = elements.filter(x => x.transactionId === filter.transactionId);
        }

        return elements;
    }

    remove(options){

        options = options || {};

        let filter = options.filter || {};

        let elements = this._cached;

        if(filter.transactionId){
            elements.filter(x => x.transactionId === filter.transactionId).forEach(x => TransactionTagManager.removeElement(x, this._cached));
        }

        this.storage.setItem(this.table, JSON.stringify(this._cached));
    }

    static removeElement(obj, elements){
        var index = elements.indexOf(obj);
        elements.splice(index, 1);
    }
}
