
import {Controller} from 'controller';
import {ControllerDialogEdit} from 'finances/dialog-edit-controller';
import {getChart as getTransactionLinearChart} from 'finances/chart-linear-transaction';
import {Presets, TransactionTableController} from 'finances/transaction-table-controller';
import * as dom from 'util/dom';
import * as util from 'util/util';
import * as currency from 'finances/model/currency';
import {Transaction} from 'finances/model/transaction';
import {TransactionTag} from 'finances/model/transaction-tag';
import {TransactionManager} from 'finances/model/transaction-manager';
import {TransactionTagManager} from 'finances/model/transaction-tag-manager';
import {TagEditor} from 'component/tag-editor';

export class MainController extends Controller {

    constructor(options){
        super(options);

        this._controllerDialogAdd = null;

        this._chart = null;

        this._transactionManager = new TransactionManager({
            table: 'transaction'
        });

        this._transactionManager.init();

        this._transactionTagManager = new TransactionTagManager({
            table: 'transaction-tag'
        });

        this._transactionTagManager.init();

        this._controllerTransactionTable = new TransactionTableController({
            parentNode: document.body,
            transactionManager: this._transactionManager,
            transactionTagManager: this._transactionTagManager,
            events: {
                onButtonEditClick: this._onTransactionTableButtonEditClick.bind(this)
            }
        }).render();

        this._controllerTransactionTable.filterByPreset(Presets.LastThirtyDays);

        this.refreshChart();
    }

    render(){

        this.domNode = dom.declare('div');

        this.options.parentNode.appendChild(this.domNode);

        this.domButtonToggleDialogAdd = dom.declare('button', {
            className: 'btn btn-success btn-add-transaction',
            textContent: '+',
            title: 'Добавить транзакцию'
        });

        this.domNode.appendChild(this.domButtonToggleDialogAdd);

        this.domButtonToggleDialogAdd.addEventListener('click', this._onButtonToggleDialogAddClick.bind(this), false);

        return this;
    }

    refreshChart(){

        if(this._chart){
            this._chart.parentNode.removeChild(this._chart);
        }

        let transactions = this._transactionManager.list();

        transactions.sort(TransactionTableController.comparatorFactory('dateCreated'));

        if(transactions.length > 1){

            this._chart = getTransactionLinearChart({
                data: transactions,
                events: {
                    onSelectionMove: util.throttle(this._onChartSelectionMove.bind(this), 150)
                }
            });

            this.options.parentNode.appendChild(this._chart);

            dom.setSelection(this._chart, false);
        }
    }

    _onChartSelectionMove(sender, args){

        let start = args.dateBegin;
        let end = args.dateEnd;

        if(start > end){
            [start, end] = [end, start];
        }

        this._controllerTransactionTable.setDatePreset(Presets.CustomDate, { dateCreatedBegin: start, dateCreatedEnd: end });
        this._controllerTransactionTable.filter({ dateCreatedBegin: start, dateCreatedEnd: end });
    }

    _onButtonToggleDialogAddClick(){

        this._controllerDialogAdd = new ControllerDialogEdit({
            parentNode: document.body,
            title: 'Добавление транзакции',
            events: {
                onHidden: function(sender){

                    sender.dispose();

                    this._controllerDialogAdd = null;

                }.bind(this),
                onSave: this._onTransactionSave.bind(this)
            }
        });

        this._controllerDialogAdd.render();
        this._controllerDialogAdd.show();
    }

    _onTransactionSave(sender, args){

        sender.hide();
        sender.dispose();

        this._controllerDialogAdd = null;

        let transaction;

        if(args.item){
            // editing
            args.item.dateChanged = new Date();

            Object.keys(args.data).forEach(x => {

                if(x !== 'tags'){
                    args.item[x] = args.data[x];
                }
            });

            this._transactionManager.save(args.item);

            // remove previous tags
            this._transactionTagManager.remove({
                filter: {
                    transactionId: args.item.id
                }
            });

            transaction = args.item;

        } else {
            // new
            transaction = new Transaction(args.data);

            this._transactionManager.add(transaction);
        }

        let transactionTags = args.data.tags.map(x => new TransactionTag({ tagId: x, transactionId: transaction.id }));

        this._transactionTagManager.add(...transactionTags);

        this._controllerTransactionTable.filterByPreset(); // currently selected by default

        this.refreshChart();
    }

    _onTransactionTableButtonEditClick(sender, args){

        this._controllerDialogAdd = new ControllerDialogEdit({
            title: 'Редактирование',
            item: this._transactionManager.get(args.transactionId),
            tags: this._transactionTagManager.list({
                filter: {
                    transactionId: args.transactionId
                }
            }).map(x => x.tagId),
            parentNode: document.body,
            events: {
                onHidden: function(sender){

                    sender.dispose();

                    this._controllerDialogAdd = null;

                }.bind(this),
                onSave: this._onTransactionSave.bind(this)
            }
        });

        this._controllerDialogAdd.render();
        this._controllerDialogAdd.show();

    }

    loadFromCsv(){

        d3.csv('/public/data/finances.csv', d => {

            delete d.id;

            d.add = (d.add === '1');
            d.sum = parseInt(d.sum, 10);
            d.dateCreated = util.dateParse(d.dateCreated);
            d.dateChanged = d.dateChanged ? util.dateParse(d.dateChanged) : null;

            let transaction = new Transaction(d);

            this._transactionManager.add(transaction);

            let transactionTags = d.title.split(';').map(x => new TransactionTag({ tagId: x, transactionId: transaction.id }));

            this._transactionTagManager.add(...transactionTags);

            return transaction;
        }, (err, rows) => {

            if(err){
                return console.log(err);
            }

            console.log('successfully loaded ' + rows.length + ' rows');

            this._controllerTransactionTable.filterByPreset();

            this.refreshChart();
        });
    }
}