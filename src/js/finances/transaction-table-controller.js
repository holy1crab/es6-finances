
import * as dom from 'util/dom';
import * as util from 'util/util';

import * as currency from 'finances/model/currency';

import {Controller} from 'controller';

export const Presets = {
    LastThirtyDays: '1',
    LastSevenDays: '2',
    LastThreeDays: '3',
    CurrentMonth: '4',
    LastMonth: '5',
    All: '6',
    CustomDate: '7'
};

export class TransactionTableController extends Controller {

    constructor(options){
        super(options);

        this.events = this.options.events || {};

        this._transactionManager = options.transactionManager;
        this._transactionTagManager = options.transactionTagManager;

        this._nodeBinding = {};

        this._presets = [
            { value: Presets.LastThirtyDays, title: 'Последние 30 дней' },
            { value: Presets.LastSevenDays, title: 'Последние 7 дней' },
            { value: Presets.LastThreeDays, title: 'Последние 3 дня' },
            { value: Presets.CurrentMonth, title: 'Текущий месяц' },
            { value: Presets.LastMonth, title: 'Прошлый месяц' },
            { value: Presets.All, title: 'Все' },
            { value: Presets.CustomDate, title: 'Другая дата' }
        ];

        this.domActivePage = null;

        var page = 1;

        Object.defineProperty(this, 'page', {
            get: function(){
                return page;
            },
            set: function(val){
                if(page !== val){
                    page = val;
                    this.pagePropertyChanged(page);
                }
            }.bind(this)
        });

        this.limit = 10;

        this.preset = Presets.LastThirtyDays;

        this.pagesNumber = 0;
    }

    render(){

        this.domNode = dom.declare('div', {
            className: 'transaction-list'
        });

        this.domHeader = dom.declare('div', {
            className: 'transaction-filter',
            innerHTML: [

                '<form class="form-inline">',
                    '<div class="form-group">',
                        '<label for="date-presets">Время</label>',
                        '<select class="form-control" id="date-presets">',
                            this._presets.map(x => '<option value="' + x.value + '">' + x.title + '</option>').join(''),
                        '</select>',
                    '</div>',
                    '<div class="form-group" id="date-created-begin-container" style="display: none;">',
                        '<label for="date-created-begin">Начало</label>',
                        '<input type="date" class="form-control" id="date-created-begin">',
                    '</div>',
                    '<div class="form-group" id="date-created-end-container" style="display: none;">',
                        '<label for="date-created-end">Конец</label>',
                        '<input type="date" class="form-control" id="date-created-end">',
                    '</div>',
                '</form>'

            ].join('')
        });

        this.domNode.appendChild(this.domHeader);

        this.domSelectDatePresets = this.domHeader.querySelector('#date-presets');
        this.domCreatedBeginContainer = this.domHeader.querySelector('#date-created-begin-container');
        this.domCreatedEndContainer = this.domHeader.querySelector('#date-created-end-container');
        this.domCreatedBegin = this.domCreatedBeginContainer.querySelector('#date-created-begin');
        this.domCreatedEnd = this.domCreatedEndContainer.querySelector('#date-created-end');


        this.domTableContainer = dom.declare('div', { className: 'transaction-table-container', style: 'min-height: 570px' });

        this.domNode.appendChild(this.domTableContainer);

        this.domTable = dom.declare('table', {
            className: 'table table-striped table-transaction',
            innerHTML: [
                '<thead>',
                    '<tr>',
                        '<th>№</th>',
                        '<th>Дата</th>',
                        '<th>Статьи</th>',
                        '<th>Сумма</th>',
                        '<th></th>',
                    '</tr>',
                '</thead>'
            ].join('')
        });

        this.domTableContainer.appendChild(this.domTable);

        this.domTbody = dom.declare('tbody');

        this.domTable.appendChild(this.domTbody);

        this.domNav = dom.declare('nav', { style: 'padding: 0 5px; min-height: 39px;' });

        this.domNode.appendChild(this.domNav);

        this.options.parentNode.appendChild(this.domNode);

        this._bindEvents();

        return this;
    }

    _bindEvents(){

        this.domSelectDatePresets.addEventListener('change', this._onSelectDatePresetsChanged.bind(this), false);
        this.domNav.addEventListener('click', this._onNavClick.bind(this), false);
        this.domTbody.addEventListener('click', this._onTbodyClick.bind(this), false);
    }

    _onTbodyClick(e){

        if(e.target.classList.contains('btn-edit')){

            var transactionId = parseInt(e.target.parentNode.parentNode.getAttribute('data-id'), 10);

            if(typeof this.options.events.onButtonEditClick === 'function'){
                this.options.events.onButtonEditClick(this, { e: e, transactionId: transactionId });
            }
        }
    }

    _onNavClick(e){

        e.preventDefault();

        var el = e.target;

        if(el.nodeName.toLowerCase() === 'a'){

            if(this.domActivePage){
                this.domActivePage.classList.remove('active');
            }

            this.domActivePage = el.parentNode;

            this.page = parseInt(el.textContent, 10);

            this.domNav.firstChild.children[this.page - 1].classList.add('active');
        }
    }

    _onSelectDatePresetsChanged(e){

        let presetValue = e.currentTarget.value;

        let display = (presetValue === Presets.CustomDate) ? 'inline-block' : 'none';
        this.domCreatedBeginContainer.style.display = display;
        this.domCreatedEndContainer.style.display = display;

        this.filterByPreset(presetValue);
    }

    filterByPreset(value){

        value = value || this.preset;

        this.preset = value;

        let begin = new Date();
        let end = new Date();

        begin.setHours(0);
        begin.setMinutes(0);
        begin.setSeconds(0);

        if(value === Presets.LastThirtyDays){
            begin.setDate(begin.getDate() - 30);
        } else if(value === Presets.LastSevenDays){
            begin.setDate(begin.getDate() - 7);
        } else if(value === Presets.LastThreeDays){
            begin.setDate(begin.getDate() - 3);
        } else if(value === Presets.CurrentMonth){
            begin.setDate(1);
        } else if(value === Presets.LastMonth){

            begin.setMonth(begin.getMonth() - 1);
            begin.setDate(1);

            end.setDate(0);
        } else if(value === Presets.All){

            this.all();

            return;
        }

        this.filter({ dateCreatedBegin: begin, dateCreatedEnd: end });
    }

    renderBody(rows){

        while(this.domTbody.firstChild){
            this.domTbody.removeChild(this.domTbody.firstChild);
        }

        rows.sort(TransactionTableController.comparatorFactory('dateCreated'));

        for(let i = 0, len = rows.length; i < len; ++i){

            let transaction = rows[i];

            let tags = this._transactionTagManager.list({
                filter: {
                    transactionId: transaction.id
                }
            }).map(x => x.tagId);

            let domTr = dom.declare('tr', {
                'data-id': transaction.id,
                innerHTML: [
                    '<td>',
                        '<span style="display: inline-block; width: 30px;">', transaction.id, '</span>',
                        '<span style="font-size: 16px; margin-left: 5px;', transaction.add ? 'color: green;' : 'color: red;', '">', transaction.add ? '&uarr;' : '&darr;', '</span>',
                    '</td>',
                    '<td>', util.dateFormat(transaction.dateCreated), '</td>',
                    '<td>', tags.map(x => '<kbd class="tag">#' + x + '</kbd>').join(''), '</td>',
                    '<td>', (transaction.add ? '' : '-'), transaction.sum, ' ', currency.getSign(transaction.currency), '</td>',
                    '<td><button class="glyphicon glyphicon-pencil btn btn-edit"></button></td>'
                ].join('')
            });

            this.domTbody.appendChild(domTr);

            this._nodeBinding[transaction.id] = { node: transaction, domNode: domTr }
        }

    }

    static renderPagination(pageNumber){

        let lis = [];

        for(let i = 1; i <= pageNumber; ++i){
            lis.push('<li><a href="#">' + i + '</a></li>');
        }

        return dom.declare('ul', {
            className: 'pagination transaction-pagination',
            innerHTML: lis.join('')
        });
    }

    all(){
        this.filter({});
    }

    filter(options){

        this.renderBody(this._transactionManager.list({
            range: {
                offset: 0,
                limit: this.limit
            },
            filter: {
                dateCreatedBegin: options.dateCreatedBegin,
                dateCreatedEnd: options.dateCreatedEnd
            }
        }));

        this._handleNavigation(this._transactionManager.count);
    }

    _handleNavigation(count){

        this.pagesNumber = Math.ceil(count / this.limit);

        var paginationList = TransactionTableController.renderPagination(this.pagesNumber);

        if(this.domNav.firstChild){
            this.domNav.removeChild(this.domNav.firstChild);
        }

        if(this.pagesNumber > 1){
            paginationList.children[0].classList.add('active');
            this.domActivePage = paginationList.children[0];
            this.domNav.appendChild(paginationList);
        }
    }

    pagePropertyChanged(val){

        console.log(val);

        this.renderBody(this._transactionManager.list({
            range: {
                offset: (val - 1) * this.limit,
                limit: this.limit
            }
        }));

    }

    setDatePreset(value, options){

        this.domSelectDatePresets.value = value;

        let display = (value === Presets.CustomDate) ? 'inline-block' : 'none';
        this.domCreatedBeginContainer.style.display = display;
        this.domCreatedEndContainer.style.display = display;

        if(value === Presets.CustomDate){

            options = options || {};

            if(options.dateCreatedBegin){
                this.domCreatedBegin.value = util.dateFormat(options.dateCreatedBegin, 'us');
            }

            if(options.dateCreatedEnd){
                this.domCreatedEnd.value = util.dateFormat(options.dateCreatedEnd, 'us');
            }
        }
    }

    static comparatorFactory(field){

        return function(x, y){

            if(x[field] < y[field]){
                return -1;
            }

            if(x[field] > y[field]){
                return 1;
            }

            return 0;
        }
    }
}