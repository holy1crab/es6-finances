
export class Controller {
    
    constructor(options = {}){

        Object.defineProperty(this, 'options', {
            value: options,
            writable: false
        });

        this.domNode = null;
    }

    dispose(){
        if(this.domNode && this.domNode.parentNode){
            this.domNode.parentNode.removeChild(this.domNode);
        }
    }
}
