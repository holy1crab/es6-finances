
export var IncFactory = {};

IncFactory.create = function (ns){

    var key = 'sequence-' + ns;
    var max = parseInt(localStorage.getItem(key) || 0, 10);

    return function (){
        localStorage.setItem(key, ++max);
        return max;
    };
};

export function dateParse(s){
    let [date, time] = s.split(' ');
    let [day, month, year] = date.split('.');
    let [hour, minute] = time ? time.split(':') : [0, 0];
    let args = [year, month - 1, day, hour, minute].map(x => parseInt(x, 10));
    return new Date(...args);
}

export function dateFormat(date, locale){

    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();

    if(locale === 'us'){ // meh
        return [year, two(month), two(day)].join('-');
    }

    return [two(day), '.', two(month), '.', year,
        date.getHours() > 0 || date.getMinutes() > 0 ? ' ' + two(date.getHours()) + ':' + two(date.getMinutes()) : ''].join('')
}

// stolen from https://remysharp.com/2010/07/21/throttling-function-calls
export function throttle(fn, threshhold, scope) {
    threshhold || (threshhold = 250);
    var last,
        deferTimer;
    return function () {
        var context = scope || this;

        var now = +new Date,
            args = arguments;
        if (last && now < last + threshhold) {
            // hold on to it
            clearTimeout(deferTimer);
            deferTimer = setTimeout(function () {
                last = now;
                fn.apply(context, args);
            }, threshhold);
        } else {
            last = now;
            fn.apply(context, args);
        }
    };
}

function two(v){
    return String(v).length === 1 ? '0' + v : v;
}