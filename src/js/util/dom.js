
export function declare(element, attributes) {
    var el = document.createElement(element);

    if (attributes) {

        for (var attr in attributes) {
            if (attributes.hasOwnProperty(attr) && attributes[attr] !== undefined) {
                if (attr === 'class' || attr === 'className') {
                    el.className = attributes[attr];
                } else if (/^(?:textContent|innerHTML|value)$/.test(attr)) {
                    el[attr] = attributes[attr];
                } else {
                    el.setAttribute(attr, attributes[attr]);
                }
            }
        }
    }

    return el;
}

export function setSelection(node, enabled) {

    if (enabled) {
        node.style.MozUserSelect = '';
        node.style.webkitUserSelect = '';
    } else {
        node.style.MozUserSelect = 'none';
        node.style.webkitUserSelect = 'none';
    }
}