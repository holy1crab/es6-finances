
import {MainController} from 'finances/main-controller'

window.controller = new MainController({
    parentNode: document.body
}).render();