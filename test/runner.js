require.config({
    baseUrl: '/',
    paths: {
        'mocha'         : '../node_modules/mocha/mocha',
        'chai'          : '../node_modules/chai/chai'
    },
    shim: {
        'chai-jquery': ['jquery', 'chai']
    }
});

define(function(require) {

    var chai = require('chai');
    var mocha = require('mocha');

    var should = chai.should();

    mocha.setup('bdd');

    require([
        'storage'
    ], function(require) {
        mocha.run();
    });

});
